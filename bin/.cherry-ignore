# Since we've disabled DRI3 completely in 10.0, this commit is no longer
# necessary.
f0f202e6b764be803470e27cba9102f14361ae22 glx: conditionaly build dri3 and present loader (v3)

# This patch makes bug #71870 worse, so it won't be cherry picked until that
# issue can be resolved.  See
# http://lists.freedesktop.org/archives/mesa-dev/2013-November/048899.html
068a073c1d4853b5c8f33efdeb481026f42e23a5 meta: fix meta clear of layered framebuffers

# This patch isn't actually necessary because that bug that it fixes isn't in
# the 10.0 branch.  See
# http://lists.freedesktop.org/archives/mesa-stable/2013-December/000500.html
a057b837ddd1c725a7504eedc53c6df05a012773 egl: add HAVE_LIBDRM define, fix EGL X11 platform

# Author requested skipping due to regressions
# Picking it would require at least also picking:
# 73c3c7e3, 3e0e9e3b, c59a605c
b2d1c579bb84a88179072a6a783f8827e218db55 glcpp: Set extension defines after resolving the GLSL version.

# These patches depend on other code not in stable branch.
# (at least 3b22146dc714b6090f7423abbc4df53d7d1fdaa9)
e190709119d8eb85c67bfbad5be699d39ad0118e mesa: Ensure that transform feedback refers to the correct program.
43e77215b13b2f86e461cd8a62b542fc6854dd1c i965/gen7: Use to the correct program when uploading transform feedback state.

# Author requested to ignore these four (since they depend on commits not in
# stable).
3313cc269bd428ca96a132d86da5fddc0f27386a i965: Add an option to ignore sample qualifier
a92e5f7cf63d496ad7830b5cea4bbab287c25b8e i965: Use sample barycentric coordinates with per sample shading
f5cfb4ae21df8eebfc6b86c0ce858b1c0a9160dd i965: Ignore 'centroid' interpolation qualifier in case of persample shading
dc2f94bc786768329973403248820a2e5249f102 i965: Ignore 'centroid' interpolation qualifier in case of persample shading

# This depends on the clear_buffer_object extensions work which is not in 10.0
# (See commit 5f7bc0c75904a40da0973329badea8497e53a26a on other branches)
aff7c5e78ab133866a90f67613508735c9b75094

# These patches are fixing code not present in 10.0
f34d75d6f69f4c0bf391e0adf1fd469601b01b04
e8d85034dad37177fce780ee3e09501e60be6e81
a61d859519d520b849c11ad5c1c1972870abd956
